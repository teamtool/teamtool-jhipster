'use strict';

angular.module('teamtoolApp')
    .controller('LogoutController', function (Auth) {
        Auth.logout();
    });
