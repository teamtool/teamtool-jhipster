/* globals $ */
'use strict';

angular.module('teamtoolApp')
    .directive('teamtoolAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
