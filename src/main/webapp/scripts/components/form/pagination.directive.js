/* globals $ */
'use strict';

angular.module('teamtoolApp')
    .directive('teamtoolAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
