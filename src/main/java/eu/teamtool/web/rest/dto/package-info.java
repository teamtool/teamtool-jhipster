/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package eu.teamtool.web.rest.dto;
